import time
import os
import sys
from copy import deepcopy
from random import random # random() generates a number from 0 to 1

size = 30
row = ['.'] * size
board = []
for _ in range(size):
    board.append(deepcopy(row))

def random_init(board):
    """ Fills the board randomly with 'o' """
    for i in range(size):
        for j in range(size):
            if random() < .5:
                board[i][j] = "o"

def is_alive(board, i, j):
    if i < 0 or i > size - 1:
        return False
    elif j < 0 or j > size - 1:
        return False
    elif board[i][j] == "o":
        return True
    else:
        return False

def neighbors(board, i , j):
    neighbor = 0
    if is_alive(board, i, j-1):
        neighbor += 1
    if is_alive(board, i, j+1):
        neighbor += 1
    if is_alive(board, i-1, j):
        neighbor += 1
    if is_alive(board, i+1, j):
        neighbor += 1
    if is_alive(board, i+1, j+1):
        neighbor += 1
    if is_alive(board, i+1, j-1):
        neighbor += 1
    if is_alive(board, i-1, j-1):
        neighbor += 1
    if is_alive(board, i-1, j+1):
        neighbor += 1
    #print(neighbor)
    return neighbor

def advance(board):
    bc = deepcopy(board)
    for i in range(len(bc)):
        for j in range(len(bc[0])):
            if neighbors(bc, i, j) < 2:
                board[i][j] = "."
            elif neighbors(bc, i, j) > 3:
                board[i][j] = "."
            if neighbors(bc, i, j) == 3:
                board[i][j] = "o"


def print_board(board):
    print()
    for i in range(size):
        if i > 0:
            print()
        for j in range(size):
            sys.stdout.write(board[i][j] + " "),
    print()


def main(board):
    i = 1
    random_init(board)
    while True:
        os.system('clear')
        print('Generation:', i)
        i += 1
        print_board(board)
        advance(board)
        time.sleep(0.2)

main(board)
